FROM python:3.7

ADD . ./

RUN pip install -r requirements.txt

ENTRYPOINT gunicorn -b :8081 demo.application:app