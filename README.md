demo python micro service

create docker image: docker build -t backend_micro_service .

to run the service : docker run -d -p 8081:8081 backend_micro_service

to check the health : curl http://localhost:8081/health