import falcon

from demo.health_check import HealthCheck

app = falcon.API()

health_check_resource = HealthCheck()
app.add_route('/health', health_check_resource)

