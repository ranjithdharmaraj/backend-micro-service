import socket

import falcon


class HealthCheck(object):
    def __init__(self):
        pass

    def on_get(self, request, response):
        hostname = socket.gethostname()

        health_response = \
                    {
                        'data':
                        {
                            'status': 'UP',
                            'hostname': hostname
                        }
                    }

        response.media = health_response
        response.status = falcon.HTTP_200
